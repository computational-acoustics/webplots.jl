#!/usr/bin/env julia

import TOML

struct BasicConfig

    font_name::String

    layout::PyCall.PyObject

    box_color::String
    grid_color::String
    axis_color::String

    function BasicConfig(config_file::String)

        config_data = TOML.parsefile(config_file)

        font_name = split(config_data["params"]["body_classes"])[1]

        layout = go.Layout(
            paper_bgcolor="rgba(0,0,0,0)",
            plot_bgcolor="rgba(0,0,0,0)"
        )

        box_color="rgba(0,0,0,1)"
        grid_color="rgba(0,0,0,0.1)"
        axis_color="rgba(0,0,0,1)"

        new(
            font_name,
            layout,
            box_color,
            grid_color,
            axis_color
        )

    end

end

function save_plot(fig::PyCall.PyObject, file_name::String)
    py.offline.plot(fig, filename=file_name, include_plotlyjs=false)
end

function save_plot_with_script(fig::PyCall.PyObject, file_name::String)

    save_plot(fig, file_name)

    file = open(file_name, "r")
    lines = readlines(file)
    close(file)

    lines_edit = ["""<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>"""; lines]

    file = open(file_name, "w")
    for l in lines_edit
        write(file, l * '\n')
    end
    close(file)

end

function canvas(basic_config::BasicConfig)

    fig = go.Figure(layout=basic_config.layout)

    fig.update_layout(font=Dict("family" => basic_config.font_name))

    fig.update_xaxes(
        showline=true,
        zeroline=false,
        mirror=true,
        linecolor=basic_config.box_color,
        gridcolor=basic_config.grid_color,
        zerolinecolor=basic_config.axis_color
        )

    fig.update_yaxes(
        showline=true,
        zeroline=false,
        mirror=true,
        linecolor=basic_config.box_color,
        gridcolor=basic_config.grid_color,
        zerolinecolor=basic_config.axis_color
        )

    return fig

end

function subplots(basic_config::BasicConfig; kwargs...)

    fig = su.make_subplots(values(kwargs)...)

    fig.update_layout(basic_config.layout)
    fig.update_layout(font=Dict("family" => basic_config.font_name))

    for r in 1:kwargs[:rows]
        for c in 1:kwargs[:cols]

            fig.update_xaxes(
                showline=true,
                zeroline=false,
                mirror=true,
                linecolor=basic_config.box_color,
                gridcolor=basic_config.grid_color,
                zerolinecolor=basic_config.axis_color,
                row=r,
                col=c
                )

            fig.update_yaxes(
                showline=true,
                zeroline=false,
                mirror=true,
                linecolor=basic_config.box_color,
                gridcolor=basic_config.grid_color,
                zerolinecolor=basic_config.axis_color,
                row=r,
                col=c
                )

        end
    end

    return fig

end

function add_line!(fig::PyCall.PyObject; kwargs...)
    fig.add_trace(go.Scatter(kwargs))
end

function add_histogram!(fig::PyCall.PyObject; kwargs...)
    fig.add_trace(go.Histogram(kwargs))
end

function add_bar!(fig::PyCall.PyObject; kwargs...)
    fig.add_trace(go.Bar(kwargs))
end

function add_heatmap!(fig::PyCall.PyObject; kwargs...)
    fig.add_trace(go.Heatmap(kwargs))
end

function add_isosurface!(fig::PyCall.PyObject; kwargs...)
    fig.add_trace(go.Isosurface(kwargs))
end

function add_line_subplot!(
    fig::PyCall.PyObject,
    row::Integer,
    col::Integer;
    kwargs...
    )
    fig.add_trace(go.Scatter(kwargs), row=row, col=col)
end
