# Init code inspired by https://discourse.julialang.org/t/calling-the-constructor-outside-a-module-works-but-inside-a-module-throws-argumenterror-ref-of-null-pyobject/42362/4

__precompile__() # this module is safe to precompile
module WebPlots

import PyCall

const py = PyCall.PyNULL()
const go = PyCall.PyNULL()
const su = PyCall.PyNULL()

function __init__()
    copy!(py, PyCall.pyimport("plotly"))
    copy!(go, PyCall.pyimport("plotly.graph_objects"))
    copy!(su, PyCall.pyimport("plotly.subplots"))
end

include("libWebPlots.jl")

export BasicConfig
export save_plot
export save_plot_with_script
export canvas
export subplots
export add_line!
export add_histogram!
export add_bar!
export add_heatmap!
export add_isosurface!
export add_line_subplot!

end
