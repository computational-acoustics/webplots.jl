using WebPlots
using Documenter

makedocs(;
    modules=[WebPlots],
    authors="Stefano Tronci <tronci.stefano@gmail.com>",
    repo="https://gitlab.com/stefano-tronci/WebPlots.jl/blob/{commit}{path}#L{line}",
    sitename="WebPlots.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://stefano-tronci.gitlab.io/WebPlots.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
