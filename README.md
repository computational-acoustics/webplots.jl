# WebPlots.jl

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://stefano-tronci.gitlab.io/WebPlots.jl/dev)
[![Build Status](https://gitlab.com/stefano-tronci/WebPlots.jl/badges/master/pipeline.svg)](https://gitlab.com/stefano-tronci/WebPlots.jl/pipelines)
[![Coverage](https://gitlab.com/stefano-tronci/WebPlots.jl/badges/master/coverage.svg)](https://gitlab.com/stefano-tronci/WebPlots.jl/commits/master)

Julia package to produce HTML plots for the [Computational Acoustics with Open Source Software website](https://computational-acoustics.gitlab.io/website/) ([repository](https://gitlab.com/computational-acoustics/website)).

# Description

This package produces interactive HTML plots by using [Plotly](https://plotly.com/) as a back-end. The Plotly python API functions are called by this package through [PyCall](https://github.com/JuliaPy/PyCall.jl).

## Why?

`Plotly` does offer a Julia interface, and many other Julia plotting packages allow to use `Plotly` as a back-end. However, the author of this package found that certain plot types (for example 3D iso-surfaces) were not supported in the Julia ecosystem. Hence, this minimal package was created.

This package is meant to read the `config.toml` configuration file of a [Hugo](https://gohugo.io/) website running on the [Ananke](https://themes.gohugo.io/gohugo-theme-ananke/) theme for best visual integration (according to the author taste).

## Should I use this Package?

This project is largely personal and this package exist for the author convenience. However, you are welcome to use it for the plots of your Hugo website if you like the style (see [here](https://computational-acoustics.gitlab.io/website/) for examples). It has to be stressed that this is not a full blown package, but mainly a collection of helper functions that are developed on a as-needed basis.

# Installation

`PyCall` is included as dependency to this package but it might be tricky to install. For his own applications, the author found easiest and most beneficial to setup a python virtual environment dedicated to Julia. This environment is to be equipped with `Plotly`. After this step is done the installation of this package should be smooth.

Refer to the step below to setup your Julia python virtual environment from scratch if you did not do so yet.

```bash
# Setting up a Python venv
python3 -m venv juliaenv
# Installing Plotly in the venv
juliaenv/bin/pip3 install plotly
```

Now, install `PyCall` by setting the `ENV["PYTHON"]` to the python binary in the newly created python virtual environment.

```julia
julia> ENV["PYTHON"]="~/juliaenv/bin/python3"
```
Press `]` to enter package mode and install and build PyCall:

```julia
(@v1.5) pkg> add PyCall
(@v1.5) pkg> build PyCall
```

From now on `PyCall` will use python from the `juliaenv`. If python packages need to be used through `PyCall`, these need to be installed in `juliaenv`.

This package can now be easily installed:

```julia
(@v1.5) pkg> add https://gitlab.com/computational-acoustics/webplots.jl.git
```

# Including a `Plotly` Plot in my Hugo Website

One simple way is to use this shortcode:

```text
{{ $fname := .Get "fname" }}
{{ readFile $fname | safeHTML }}
```

